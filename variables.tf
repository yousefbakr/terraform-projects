variable "vpc_cider_block" {
    type=string
    default="10.0.0.0/16"
}

variable "subnet_cider_block"{
    type=string
    default="10.0.10.0/24"
}

variable "avail_zone"{
    type=string
    default="eu-west-3b"
}

variable "env_prefix"{
    type="string"
    default="dev"
}

variable "instance_type"{
type="string"
default="t2.micro"
}


variable "myip"{
type="string"
default="192.168.44.76/32"
}

