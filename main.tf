

resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cider_block
    tags = {
      Name:"${var.env_prefix}-vpc"
}
}

resource "aws_subnet" "dev-subent-1" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cider_block
  availability_zone = var.avail_zone
  tags = {
    Name:"${var.env_prefix}-subnet-1"}
}

resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp-vpc.id
  route {
      cidr_block="0.0.0.0/0"
      gateway_id=aws_internet_gateway.myapp-igw.id
  }
  tags = {
    "Name" = "${var.env_prefix}-rtb"
  }
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myapp-vpc.id
  tags = {
    Name :"${var.env_prefix}-igw"
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id = aws_subnet.dev-subent-1.id
  route_table_id = aws_route_table.myapp-route-table.id
  
}

resource "aws_security_group" "myapp-sg" {
  name="myapp-sg"
  vpc_id = aws_vpc.myapp-vpc.id

  ingress   {
    from_port= 22
    to_port= 22
    protocol="tcp"
    cidr_blocks = [var.myip]
  } 

  ingress {
    from_port= 8080
    to_port= 8080
    protocol="tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }


  egress {
    cidr_blocks = [ "0.0.0.0/0"]
    from_port = 0
    prefix_list_ids = []
    protocol = "-1"
    to_port = 0
  } 
  
  tags = {
    "Name" = "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = [ "amazon" ]

  filter {
    name="name"
    values = [ "amzn2-ami-hvm-*-x86_64-gp2" ]
  }
  filter {
    name="virtualization-type"
    values = [ "hvm" ]
  }

}


output "aws_ami_id" {
  value = data.aws_ami.latest-amazon-linux-image.id
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type
}